﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Configuration;

namespace PayMe.Helpers
{
    public class GetTransactions //Deze klasse is een test, niet echt belangrijk aangeizen het niet werkte.
    {
        private static string connstring = "";
        private static IConfiguration _configuration;
        public GetTransactions(IConfiguration configuration)
        {
            _configuration = configuration;
            connstring = _configuration.GetConnectionString("UserDBcon");
        }

        public static void showTransactions()
        {

            using (SqlConnection cnn = new SqlConnection(connstring))
            {

                string query = "SELECT * FROM dbo.TransactionHistory";

                using (SqlCommand command = new SqlCommand(query, cnn))
                {
                    cnn.Open();

                    int i = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            i++;
                            System.Diagnostics.Debug.WriteLine(i);
                        }
                    }

                    cnn.Close();
                }
            }
        }
    }
}