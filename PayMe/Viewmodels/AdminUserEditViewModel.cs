﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PayMe.Viewmodels
{
    public class AdminUserEditViewModel
    {

        public AdminUserEditViewModel()
        {
            Roles = new List<string>();
        }
        public IList<string> Roles { get; set; }

        public string Id { get; set; }
        public string UserName { get; set; }

        [EmailAddress]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
            ErrorMessage = "Email must be a valid Address Email.")]
        public string Email { get; set; }
        public decimal Money { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UniqueCode { get; set; }
    }
}
