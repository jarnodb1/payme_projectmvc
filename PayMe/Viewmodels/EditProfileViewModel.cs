﻿using System.ComponentModel.DataAnnotations;

namespace PayMe.Viewmodels
{
    public class EditProfileViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }

        [EmailAddress]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
            ErrorMessage = "Email must be a valid Address Email.")]
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
