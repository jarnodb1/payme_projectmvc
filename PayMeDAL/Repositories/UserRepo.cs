﻿using Microsoft.EntityFrameworkCore;
using PayMeDAL.Context;
using PayMeDAL.Models;
using System;
using System.Collections.Generic;

namespace PayMeDAL.Repositories
{
    public class UserRepo : IUserRepo
    {
        private readonly AppDbContext _context;

        public UserRepo()
        {
        }

        public UserRepo(AppDbContext context)
        {
            _context = context;
        }

        public ApplicationUser Delete(int id)
        {
            var user = _context.Users.Find(id);

            if (user != null)
            {
                var deleteduser = _context.Users.Remove(user);

                if (deleteduser != null && deleteduser.State == EntityState.Deleted)
                {
                    var affectedRows = _context.SaveChanges();

                    if (affectedRows > 0)
                    {
                        return deleteduser.Entity;
                    }
                }
            }
            return null;
        }

        public IEnumerable<ApplicationUser> GetAll()
        {
            var listUsers = _context.Users;
            return listUsers;
        }

        public ApplicationUser GetById(int Id)
        {
            var user = _context.Users.Find(Id);
            return user;
        }

        public ApplicationUser Update(ApplicationUser user)
        {
            var newuser = _context.Users.Update(user);

            if (newuser != null && newuser.State == EntityState.Modified)
            {
                var affectedRows = _context.SaveChanges();

                if (affectedRows > 0)
                {
                    return newuser.Entity;
                }
            }
            return null;
        }
    }
}