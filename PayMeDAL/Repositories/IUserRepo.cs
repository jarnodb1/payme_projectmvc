﻿using PayMeDAL.Models;
using System.Collections.Generic;

namespace PayMeDAL.Repositories
{
    public interface IUserRepo
    {
        ApplicationUser GetById(int Id);

        IEnumerable<ApplicationUser> GetAll();

        ApplicationUser Update(ApplicationUser user);

        ApplicationUser Delete(int id);
    }
}
