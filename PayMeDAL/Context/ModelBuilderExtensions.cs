﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PayMeDAL.Models;
using System;

namespace PayMeDAL.Context
{
    public static class ModelBuilderExtensions
    {
        private static PasswordHasher<ApplicationUser> passwordHasher = new PasswordHasher<ApplicationUser>();
        public static void Seed(this ModelBuilder modelBuilder)
        {
            //Creating the admin account
            ApplicationUser jarno = new ApplicationUser();

            jarno.Id = Guid.NewGuid().ToString();
            jarno.Email = "a@a.com";
            jarno.UserName = "a@a.com";
            jarno.NormalizedEmail = "A@A.COM";
            jarno.NormalizedUserName = "A@A.COM";
            jarno.PasswordHash = passwordHasher.HashPassword(jarno, "Server2016!");
            jarno.UniqueCode = "AZERTY";
        
            modelBuilder.Entity<ApplicationUser>().HasData(jarno);

            //Creating admin role
            modelBuilder.Entity<IdentityRole>().HasData(
          new IdentityRole
          {
              Name = "Admin",
              Id = "1",
              NormalizedName = "ADMIN"
          }
          );

            //setting admin account to admin role
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
          new IdentityUserRole<string>
          {
              UserId = jarno.Id,
              RoleId = "1"
          }
          );

            

            modelBuilder.Entity<ApplicationUser>().HasData(
                new ApplicationUser() { FirstName = "Wim", LastName = "De Leerkracht", UserName = "w@w.com", Email = "w@w.com", NormalizedEmail = "W@W.COM", NormalizedUserName = "W@W.COM", Money = 50, PasswordHash = jarno.PasswordHash },                               
                new ApplicationUser() { FirstName = "Jens", LastName = "Van de Steen", UserName = "j@j.com", Email = "j@j.com", NormalizedEmail = "J@J.COM", NormalizedUserName = "J@J.COM", Money = 50, PasswordHash = jarno.PasswordHash },                             
                new ApplicationUser() { FirstName = "Dennis", LastName = "De Backer", UserName = "d@d.com", Email = "d@d.com", NormalizedEmail = "D@D.COM", NormalizedUserName = "D@D.COM", Money = 50, PasswordHash = jarno.PasswordHash },        
                new ApplicationUser() { FirstName = "Lena", LastName = "Gonzalo", UserName = "l@l.com", Email = "l@l.com", NormalizedEmail = "L@L.COM", NormalizedUserName = "L@L.COM", Money = 50, PasswordHash = jarno.PasswordHash },
                new ApplicationUser() { FirstName = "Maite", LastName = "Isrijk", UserName = "m@m.com", Email = "m@m.com", NormalizedEmail = "M@M.COM", NormalizedUserName = "M@M.COM", Money = 5000, PasswordHash = jarno.PasswordHash }
                );


        }
    }
}
